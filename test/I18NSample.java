package test;

import java.util.Locale;
import java.util.ResourceBundle;

public class I18NSample {

    static public void main(String[] args) {

        Locale currentLocale;
        ResourceBundle messages;

        currentLocale = new Locale(System.getProperty("user.language"), System.getProperty("user.country"));

        messages = ResourceBundle.getBundle("MessagesBundle", currentLocale);
        System.out.println(messages.getString("greetings"));
        System.out.println(messages.getString("inquiry"));
        System.out.println(messages.getString("farewell"));
        
        
    }
}